import os
import wget
import numpy as np
from calendar import monthrange
from datetime import datetime, timedelta
from xarray import open_mfdataset, open_dataset, DataArray
from mpl_toolkits.basemap import shiftgrid
from utils import interpolate2grid


SST_DIR = './data/oisst/'
NCEP_DIR = f'data/ncep1/'
SSTCLIM_DIR = 'data/oisst/8210/'
NCEPCLIM_DIR = f'data/ncep1/8210/'
CLIM_DIR = f'data/clims/'

lat_ = np.arange(-90., 91., 1.)
lon_ = np.arange(-180., 180., 1.)


def download_flux_8210_data():

    files_prefix = ['dswrf.sfc.gauss.', 'uswrf.sfc.gauss.', 'dlwrf.sfc.gauss.',
                    'ulwrf.sfc.gauss.', 'shtfl.sfc.gauss.', 'lhtfl.sfc.gauss.']
    land_mask = open_dataset(f'{NCEP_DIR}land.sfc.gauss.nc')['land'].values
    file_list = []
    # Download data
    for i in files_prefix:
        for j in np.arange(1982, 2011):
            name = f'{i}{j}.nc'
            link = 'ftp://ftp2.psl.noaa.gov/Datasets/ncep.reanalysis.dailyavgs/surface_gauss/'
            if not os.path.exists(f'{NCEPCLIM_DIR}{i}{j}.nc'):
                print(f'{NCEPCLIM_DIR}{i}{j}.nc')
                wget.download(link+name, f'{NCEPCLIM_DIR}{i}{j}.nc')
                print()

        varname = i.split('.')[0]
        varfile = f'{NCEPCLIM_DIR}{varname}.daily.1dg.1982-2010.nc'
        if not os.path.exists(varfile):
            ds = open_mfdataset(f'{NCEPCLIM_DIR}{i}*.nc', concat_dim='time')
            var = ds[varname]
            var_masked = np.where(np.tile(land_mask, (var.shape[0], 1, 1))==1, np.nan, var)
            var_shift, lon_shift = shiftgrid(180.185, var_masked[:], ds['lon'].values, False)
            var_interp = interpolate2grid(var_shift[:, ::-1, :], ds['lat'].values[::-1], lon_shift, lat_, lon_)
            DataArray(var_interp, dims=['time', 'lat', 'lon'], coords={'time': var['time'].values, 'lat': lat_, 'lon': lon_}, name=varname).to_netcdf(varfile)
            ds.close()
            del var, var_masked, var_shift, var_interp
        file_list.append(varfile)

    # Calcula o surface net flux diário e salva em netCDF
    varfile = f'{NCEPCLIM_DIR}sfcflux.daily.1dg.1982-2010.nc'
    if not os.path.exists(varfile):
        ds = open_mfdataset(file_list, concat_dim='time')

        F = np.zeros(ds['uswrf'].shape)
        
        for n, i in enumerate(ds['time'].values):
            net_short = ds['uswrf'].loc[i] - ds['dswrf'].loc[i]
            net_long = ds['ulwrf'].loc[i] - ds['dlwrf'].loc[i]
            shf = ds['shtfl'].loc[i]
            lhf = ds['lhtfl'].loc[i]
            F[n] = (net_short + net_long + shf + lhf) * -1.

        DataArray(F, dims=['time', 'lat', 'lon'], coords={'time': ds['time'].values, 'lat': ds['lat'].values, 'lon': ds['lon'].values}, name='flux').to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


def compute_flux_climatology():

    varfile = f'{CLIM_DIR}sfcflux.daily.clim.1dg.1982-2010.nc'
    if not os.path.exists(varfile):
        ds = open_dataset(f'{NCEPCLIM_DIR}sfcflux.daily.1dg.1982-2010.nc')
        F = ds['flux'].groupby("time.dayofyear").mean("time")
        lat = F['lat'].values
        lon = F['lon'].values
        F = DataArray(F.values, dims=['dayofyear', 'lat', 'lon'], coords={'dayofyear': F['dayofyear'].values, 'lat': lat, 'lon': lon}, name='flux')
        F.to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


def download_sst_8210_data():

    SST_REP = 'https://www.ncei.noaa.gov/data/sea-surface-temperature-optimum-interpolation/v2.1/access/avhrr/'
    prefix = 'oisst-avhrr-v02r01.'

    for i in range(1982, 2011):
        for j in range(1, 13):
            ndays = monthrange(i, j)[1]
            for l in range(1, ndays+1):
                if not os.path.exists(f'{SSTCLIM_DIR}{prefix}{i}{j:02d}{l:02d}.nc'):
                    wget.download(f'{SST_REP}{i}{j:02d}/{prefix}{i}{j:02d}{l:02d}.nc', f'{SSTCLIM_DIR}{prefix}{i}{j:02d}{l:02d}.nc')
                    ds = open_dataset(f'{SSTCLIM_DIR}{prefix}{i}{j:02d}{l:02d}.nc')
                    sst_shift, lon_shift = shiftgrid(180.125, ds['sst'].values[:, :, :, :], ds['lon'].values, False)
                    ds['sst'].values = sst_shift
                    sst = ds['sst'].assign_coords(lon=lon_shift).interp(lat=lat_, lon=lon_)
                    ds.close()
                    sst.to_netcdf(f'{SSTCLIM_DIR}{prefix}{i}{j:02d}{l:02d}.nc')
                    del sst, sst_shift
                    print()

    varfile = f'{SSTCLIM_DIR}sst.daily.1dg.1982-2010.nc'
    if not os.path.exists(varfile):
        ds = open_mfdataset(f'{SSTCLIM_DIR}{prefix}*.nc', concat_dim='time')
        ds['sst'].squeeze('zlev').drop('zlev').to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


def compute_sst_climatology():

    varfile = f'{CLIM_DIR}sst.daily.clim.1dg.1982-2010.nc'
    if not os.path.exists(varfile):
        ds = open_mfdataset(f'{SSTCLIM_DIR}sst.daily.1dg.1982-2010.nc')
        sst = ds['sst'].groupby("time.dayofyear").mean("time")
        lat = ds['lat'].values
        lon = ds['lon'].values
        # DEIXAR EIXO DO TEMPO COM 366 E DIM COMO DAYOFYEAR
        sstclim = DataArray(sst.values, dims=['dayofyear', 'lat', 'lon'], coords={'dayofyear': sst['dayofyear'].values, 'lat': lat, 'lon': lon}, name='sst')
        sstclim.to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


def compute_mld_climatology():

    varfile = f'{CLIM_DIR}mld.daily.clim.1dg.1990-1992.nc'
    if not os.path.exists(varfile):
        MLDCLIM_DIR = 'data/levitus_mld/'
        list_files = ['mld_jan.nc', 'mld_fev.nc', 'mld_mar.nc', 'mld_abr.nc', 'mld_mai.nc', 'mld_jun.nc',
                    'mld_jul.nc', 'mld_ago.nc', 'mld_set.nc', 'mld_out.nc', 'mld_nov.nc', 'mld_dez.nc']
        clim_mon = np.zeros((12, 180, 360))
        for n, i in enumerate(list_files):
            ds = open_dataset(f'{MLDCLIM_DIR}{i}')
            clim_mon[n] = ds['mld'].values
            lat = ds['lat'].values
            lon = ds['lon'].values
        clim_mon = np.concatenate((clim_mon[11][np.newaxis, ...], clim_mon, clim_mon[0][np.newaxis, ...]), 0)
        timeaxis_day = np.arange(datetime(2019, 12, 15), datetime(2021, 1, 16), timedelta(days=1), dtype=datetime)
        timeaxis_mon = [i for i in timeaxis_day if i.day == 15]

        ds_mon = DataArray(clim_mon,  dims=['time', 'lat', 'lon'], coords={'time': timeaxis_mon, 'lat': lat, 'lon': lon}, name='mld')
        ds_day = ds_mon.interp(time=timeaxis_day).loc['2020-01-01':'2020-12-31']
        ds_day = ds_day.assign_coords({'time': ds_day['time.dayofyear'].values}).rename({'time':'dayofyear'})
        ds_day = ds_day.interp(lat=np.arange(-90., 91., 1), lon=np.arange(-180., 180., 1.))

        ds_day.to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


def compute_ocean_flux_cimatology():

    varfile = f'{CLIM_DIR}ocflux.daily.clim.1dg.1982-2010.nc'
    if not os.path.exists(varfile):
        dummy_time = np.arange(datetime(2020, 1, 1), datetime(2021, 1, 1), timedelta(days=1), dtype=datetime)
        ds_sst = open_dataset(f'{CLIM_DIR}sst.daily.clim.1dg.1982-2010.nc')
        sst = ds_sst['sst'].assign_coords({'dayofyear': dummy_time}).groupby('dayofyear.month').mean('dayofyear')
        ds_sflux = open_dataset(f'{CLIM_DIR}sfcflux.daily.clim.1dg.1982-2010.nc')
        F = ds_sflux['flux'].assign_coords({'dayofyear': dummy_time}).groupby('dayofyear.month').mean('dayofyear')
        ds_mld = open_dataset(f'{CLIM_DIR}mld.daily.clim.1dg.1990-1992.nc')
        h = ds_mld['mld'].assign_coords({'dayofyear': dummy_time}).groupby('dayofyear.month').mean('dayofyear')

        rho = 1026 
        Co = 3930
        Qo = np.zeros(sst.shape)
        Q = np.zeros(sst.shape)
        for n in range(0, 12):
            ndays = monthrange(2020, n+1)[1]
            if n == 0:
                Qo[n] = (sst[n+1] - sst[11]) * (rho*Co*h[n]/(ndays*86400))
            elif n == 11:
                Qo[n] = (sst[0] - sst[n-1]) * (rho*Co*h[n]/(ndays*86400))
            else:
                Qo[n] = (sst[n+1] - sst[n-1]) * (rho*Co*h[n]/(ndays*86400))
            
            Q[n] = Qo[n] - F[n]

        timeaxis_day = np.arange(datetime(2019, 12, 15), datetime(2021, 1, 16), timedelta(days=1), dtype=datetime)
        timeaxis_mon = [i for i in timeaxis_day if i.day == 15]
        Q_mon = np.concatenate((Q[-1][np.newaxis], Q, Q[0][np.newaxis]))
        Q = DataArray(Q_mon, dims=['time', 'lat', 'lon'], coords=({'time': timeaxis_mon, 'lat': sst['lat'].values, 'lon': sst['lon'].values}), name='flux')
        Q = Q.interp(time=timeaxis_day).loc['2020-01-01':'2020-12-31']
        Q = Q.assign_coords({'time': Q['time.dayofyear'].values}).rename({'time':'dayofyear'})

        Q.to_netcdf(varfile)
        print (f'File {varfile} was created')
    else:
        print(f'File {varfile} already exists!')


download_flux_8210_data()
compute_flux_climatology()
download_sst_8210_data()
compute_sst_climatology()
compute_mld_climatology()
compute_ocean_flux_cimatology()
