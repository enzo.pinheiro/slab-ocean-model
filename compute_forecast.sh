#!/bin/bash
set -e

indate=$1
ndays=$2
nlead=$3
delete=0  # Switch here to 1 if you want created files to be deleted by the end of the process
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dayback=$(date --date="$indate - $ndays day" +%Y%m%d)

DATA_DIR="${SCRIPT_DIR}/data/forecasts/${indate}_${dayback}"

echo "Preparing observational data ..."
python3 preprocessing.py --date $indate --daysback $ndays
echo "Computing forecast ..."
python3 ssta_forecast.py --date $indate --daysback $ndays --lead $nlead
echo "Done! results are in data/results/forecasts/"
if [ $delete -eq 1 ]; then
    rm -r $DATA_DIR
fi
