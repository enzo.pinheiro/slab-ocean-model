#!/bin/bash
set -e

ndays=$1
nlead=$2
delete=0  # Switch here to 1 if you want created files to be deleted by the end of the process
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DATA_DIR="${SCRIPT_DIR}/data/8210/"

echo "Preparing observational data ..."
python3 preprocessing_hindcast.py --daysback $ndays --lead $nlead
echo "Computing hindcast ..."
python3 ssta_hindcast.py --daysback $ndays --lead $nlead
echo "Creating verification figures ..."
python3 hindcast_verification.py --daysback $ndays --lead $nlead
echo "Done!"

if [ $delete -eq 1 ]; then
    rm $DATA_DIR*${ndays}dmn.1dg.1982-2010.nc
    rm $DATA_DIR*${ndays}dmn.${nlead}dleadmn.1dg.1982-2010.nc
fi
