import os
import argparse
import numpy as np
from netCDF4 import Dataset
from mpl_toolkits.basemap import shiftgrid
from xarray import DataArray, corr
from matplotlib import pyplot as plt
from utils import plot_field, rmse, DIR_8210, FIGS_HIND_DIR


def arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("--daysback", type=str, help="days back from init date")
    parser.add_argument("--lead", type=str, help="lead time in days")

    return parser.parse_args()


args = arguments()
ndays = int(args.daysback)
lead = int(args.lead)

cnc = Dataset(f'{DIR_8210}sst.anom.{ndays}dmn.1dg.1982-2010.nc')
clim = cnc.variables['sst'][1:]
latc = cnc.variables['lat'][:]
lonc = cnc.variables['lon'][:]
timec = cnc.variables['time'][1:]  # Primeiro passo de tempo é ignorado pq o hindcast começa na segunda semana
clim = DataArray(clim[:], dims=['time', 'lat', 'lon'], coords={'time': timec[:], 'lat': latc, 'lon': lonc})  

hnc = Dataset(f'{DIR_8210}sst.anom.hindcast.{ndays}dmn.{lead}dleadmn.1dg.1982-2010.nc')
hind = hnc.variables['sst'][1:, :, :]
lath = hnc.variables['lat'][:]
lonh = hnc.variables['lon'][:]
timeh = hnc.variables['time'][1:]  # Primeiro passo de tempo é ignorado pq o hindcast começa na segunda semana
hind = DataArray(hind[:], dims=['time', 'lat', 'lon'], coords={'time': timeh[:], 'lat': lath, 'lon': lonh})  

correl = corr(clim, hind, 'time')
rmse = rmse(hind, clim, 0)
std_hind = np.nanstd(hind, 0)
std_clim = np.nanstd(clim, 0)
std_ratio = std_hind/std_clim

p33 = np.percentile(clim, 33.3, 0)
p66 = np.percentile(clim, 66.6, 0)

clim_bellow = np.where(clim < p33, 1, 0)
clim_normal = np.where(((clim >= p33) & (clim <= p66)), 1, 0)
clim_above = np.where(clim > p66, 1, 0)

hind_bellow = np.where(hind < p33, 1, 0)
hind_normal = np.where(((hind >= p33) & (hind <= p66)), 1, 0)
hind_above = np.where(hind > p66, 1, 0)

# Tabela de contingência
TA = np.nansum(((hind_above==1) & (clim_above==1)), 0)
TN = np.nansum(((hind_normal==1) & (clim_normal==1)), 0)
TB = np.nansum(((hind_bellow==1) & (clim_bellow==1)), 0)
FA_N = np.nansum(((hind_above==1) & (clim_normal==1)), 0)
FA_B = np.nansum(((hind_above==1) & (clim_bellow==1)), 0)
FN_A = np.nansum(((hind_normal==1) & (clim_above==1)), 0)
FN_B = np.nansum(((hind_normal==1) & (clim_bellow==1)), 0)
FB_N = np.nansum(((hind_bellow==1) & (clim_normal==1)), 0)
FB_A = np.nansum(((hind_bellow==1) & (clim_above==1)), 0)

pA = TA/(TA+FA_N+FA_B)
pN = TN/(FN_B+TN+FN_A)
pB = TB/(FB_A+FB_N+TB)

rA = TA/(TA+FN_A+FB_A)
rN = TN/(FA_N+TN+FB_N)
rB = TB/(FA_B+FN_B+TB)

p = (pA + pN + pB)/3  # precision / hit rate
r = (rA + rN + rB)/3  # recall / probability of detection
f1 = (2*p*r)/(p+r)

fig = plt.figure(figsize=(12, 6))
gs = fig.add_gridspec(2, 2)
plt.suptitle(f'Hindcast verification (82-10) - Lead avg: {lead} days - Past avg: {ndays} days')

ax1 = fig.add_subplot(gs[0, 0])
plot_field(correl, lath, lonh, np.arange(-1., 1.25, 0.25), cmap=plt.set_cmap('RdBu_r'), extend=None, title='correlação', ax=ax1)

ax2 = fig.add_subplot(gs[0, 1])
plot_field(rmse, lath, lonh, np.arange(0., 1.5, 0.1), cmap=plt.set_cmap('Greens'), extend='max', title='rmse', ax=ax2)

ax3 = fig.add_subplot(gs[1, 0])
plot_field(std_ratio, lath, lonh, np.arange(0.25, 2., 0.25), cmap=plt.set_cmap('RdBu_r'), extend='both', title='razão dos desvios', ax=ax3)

ax4 = fig.add_subplot(gs[1, 1])
plot_field(f1, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('winter_r'), extend=None, title='f1 score', ax=ax4)

plt.savefig(f'{FIGS_HIND_DIR}hind_{ndays}daysmn_{lead}daysfore_verification.png')

fig = plt.figure(figsize=(12, 6))
gs = fig.add_gridspec(2, 3)
plt.suptitle(f'Hindcast verification (82-10) - Lead avg: {lead} days - Past avg: {ndays} days')


plt.figtext(0.5, 0.89, 'Probability of Detection (Precision)', fontsize=14, ha='center')
ax1 = fig.add_subplot(gs[0, 0])
plot_field(pA, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Above Category', ax=ax1)

ax2 = fig.add_subplot(gs[0, 1])
plot_field(pN, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Normal Category', ax=ax2)

ax3 = fig.add_subplot(gs[0, 2])
plot_field(pB, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Below Category', ax=ax3)

plt.figtext(0.5, 0.46, 'Frequency of Hit (Recall)', fontsize=14, ha='center')
ax4 = fig.add_subplot(gs[1, 0])
plot_field(rA, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Above Category', ax=ax4)

ax5 = fig.add_subplot(gs[1, 1])
plot_field(rN, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Normal Category', ax=ax5)

ax6 = fig.add_subplot(gs[1, 2])
plot_field(rB, lath, lonh, np.arange(0., 1.1, 0.1), cmap=plt.set_cmap('Greys'), extend=None, title='Below Category', ax=ax6)

plt.savefig(f'{FIGS_HIND_DIR}hind_{ndays}daysmn_{lead}daysfore_verification_precision_recall.png')
