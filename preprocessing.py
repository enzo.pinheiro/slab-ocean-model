import os
import wget
import requests
import argparse
import numpy as np
from datetime import datetime, timedelta
from mpl_toolkits.basemap import shiftgrid
from dateutil.relativedelta import relativedelta
from xarray import open_mfdataset, open_dataset, concat, DataArray
from utils import interpolate2grid, FLX_DIR, SST_DIR, CLIM_DIR, FORE_DIR


lat2interp = np.arange(-90., 91., 1.)
lon2interp = np.arange(-180., 180., 1.)


def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--date", type=str, help="date to initialize the model in the format YYYYMMDD")
    parser.add_argument("--daysback", type=int, help="days back from init date")

    return parser.parse_args()


def create_sfcflux_data(idate, fdate, ndays, OUT_DIR):

    '''
    Downloads monthly flux data, computes surface net flux and saves it in a netcdf for the chosen dates.
    idate (datetime)
    fdate (datetime)
    OUT_DIR (str)
    '''

    url = 'ftp://ftp.cdc.noaa.gov/Datasets/ncep.reanalysis.dailyavgs/surface_gauss/'
    iyear = idate.year
    fyear = fdate.year
    years = np.arange(iyear, fyear+1)
    files_prefix = ['dswrf.sfc.gauss.', 'uswrf.sfc.gauss.', 'dlwrf.sfc.gauss.',
                    'ulwrf.sfc.gauss.', 'shtfl.sfc.gauss.', 'lhtfl.sfc.gauss.']
    land_mask = open_dataset(f'{FLX_DIR}land.sfc.gauss.nc')['land'].values
    for j in files_prefix:
        for i in years:
            if not os.path.exists(f'{FLX_DIR}{j}{i}.nc') or i == datetime.now().year:
                print(f'Downloading {j}{i}.nc')
                if os.path.exists(f'{FLX_DIR}{j}{i}.nc'):
                    os.remove(f'{FLX_DIR}{j}{i}.nc')
                wget.download(f'{url}{j}{i}.nc', f'{FLX_DIR}{j}{i}.nc')
                print()

            if i == years[-1]:
                varname = j.split('.')[0]
                varfile = f"{OUT_DIR}{varname}_{idate.strftime('%Y%m%d')}_{fdate.strftime('%Y%m%d')}.nc"
                if not os.path.exists(varfile):
                    ds = open_mfdataset(f'{FLX_DIR}{j}*.nc', concat_dim='time')
                    if ds['time'].values[-1] < np.datetime64(fdate):
                        print ('Chosen date not available, please select a previous date')
                        exit(1)
                    var = ds[varname].loc[idate:fdate]
                    var_masked = np.where(np.tile(land_mask, (var.shape[0], 1, 1))==1, np.nan, var)
                    var_shift, lon2interpshift = shiftgrid(180.185, var_masked[:], ds['lon'].values, False)
                    var_interp = interpolate2grid(var_shift[:, ::-1, :], ds['lat'].values[::-1], lon2interpshift, lat2interp, lon2interp)
                    DataArray(var_interp, dims=['time', 'lat', 'lon'], coords={'time': var['time'].values, 'lat': lat2interp, 'lon': lon2interp}, name=varname).to_netcdf(varfile)
                    ds.close()

    climmnfile = f"{OUT_DIR}sfcflmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    anommnfile = f"{OUT_DIR}sfcflmn.anom.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    climfile = f"{CLIM_DIR}sfcflux.daily.clim.1dg.1982-2010.nc"
    if not os.path.exists(anommnfile):
        clim_ds = open_dataset(climfile)
        fluxc = concat((clim_ds['flux'][-(ndays//2):], clim_ds['flux'], clim_ds['flux'][:ndays//2]), dim='dayofyear')
        fluxc = fluxc.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]

        ds = open_mfdataset(f"{OUT_DIR}*_{idate.strftime('%Y%m%d')}_{fdate.strftime('%Y%m%d')}.nc")

        net_short = (ds['uswrf'].values-ds['dswrf'].values)
        net_long = (ds['ulwrf'].values-ds['dlwrf'].values)
        shf = ds['shtfl'].values
        lhf = ds['lhtfl'].values
        F = (net_short+net_long+shf+lhf)*-1.
        flux = DataArray(F, dims=['time', 'lat', 'lon'], coords={'time':ds['time'].values, 'lat': lat2interp, 'lon': lon2interp}, name='flux')
        flux = flux.resample(time=f'{ndays}D', skipna=True, loffset=timedelta((ndays//2))).mean('time')
        doy_centered = flux['time.dayofyear'].values
        fluxc = fluxc.loc[doy_centered]

        fluxa = flux.values - fluxc.values

        DataArray(np.nanmean(fluxc, 0), dims=['lat', 'lon'], coords={'lat': lat2interp, 'lon': lon2interp}, name='flux').to_netcdf(climmnfile)
        DataArray(np.nanmean(fluxa, 0), dims=['lat', 'lon'], coords={'lat': lat2interp, 'lon': lon2interp}, name='flux').to_netcdf(anommnfile)


def create_mnsst_data(idate, fdate, ndays, OUT_DIR):

    '''
    '''

    url = 'https://www.ncei.noaa.gov/data/sea-surface-temperature-optimum-interpolation/v2.1/access/avhrr/'
    dates_range = np.arange(idate, fdate+timedelta(days=1), timedelta(days=1), dtype=datetime)
    file_prefix = 'oisst-avhrr-v02r01.'
    files = []
    for i in dates_range:
        filename = f"{file_prefix}{i.strftime('%Y%m%d')}.nc"
        files.append(SST_DIR+filename)
        link = f"{url}{i.year}{i.month:02d}/{filename}"
        if not os.path.exists(SST_DIR+filename):
            if requests.get(link).status_code == 404:
                filename2 = filename.split('.')[0]+'.'+filename.split('.')[1]+'_preliminary'+'.nc'
                link = f"{url}{i.year}{i.month:02d}/{filename2}"
            print (f'Downloading {filename}')
            wget.download(link, SST_DIR+filename)
            print()
        else:
            continue

    climmnfile = f"{OUT_DIR}sstmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    anomnmfile = f"{OUT_DIR}sstmn.anom.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    climfile = f"{CLIM_DIR}sst.daily.clim.1dg.1982-2010.nc"
    if not os.path.exists(anomnmfile):
        
        clim_ds = open_dataset(climfile)
        sstc = clim_ds['sst']
        sstc = concat((clim_ds['sst'][-(ndays//2):], clim_ds['sst'], clim_ds['sst'][:ndays//2]), dim='dayofyear')
        sstc = sstc.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]
        
        ds = open_mfdataset(files, concat_dim='time')
        var = ds['sst'].loc[idate:fdate+timedelta(days=1)] # +1 dia pq o horário no eixo do tempo é 12h
        sst_shift, lon2interpshift = shiftgrid(180.125, var.values, ds['lon'].values, False)
        sst = interpolate2grid(sst_shift[:, 0, :, :], ds['lat'].values[:], lon2interpshift, lat2interp, lon2interp)
        sst = DataArray(sst, dims=['time', 'lat', 'lon'], coords={'time':var['time'].values, 'lat': lat2interp, 'lon': lon2interp}, name='sst')
        sst = sst.resample(time=f'{ndays}D', skipna=True, loffset=timedelta((ndays//2))).mean('time')

        doy_centered = sst['time.dayofyear'].values
        sstc = sstc.loc[doy_centered]

        ssta = sst.values - sstc.values

        DataArray(np.nanmean(sstc, 0), dims=['lat', 'lon'], coords={'lat': sst['lat'], 'lon': sst['lon']}, name='sst').to_netcdf(climmnfile)
        DataArray(np.nanmean(ssta, 0), dims=['lat', 'lon'], coords={'lat': sst['lat'], 'lon': sst['lon']}, name='sst').to_netcdf(anomnmfile)


def create_mld_data(idate, fdate, OUT_DIR):

    climmnfile = f"{OUT_DIR}mldmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    if not os.path.exists(climmnfile):

        mldclim = open_dataset(f'{CLIM_DIR}mld.daily.clim.1dg.1990-1992.nc')
        idoy = idate.timetuple().tm_yday
        fdoy = fdate.timetuple().tm_yday

        if idoy > fdoy:
            mld = concat((mldclim['mld'][idoy:], mldclim['mld'][:fdoy]), dim='dayofyear').mean('dayofyear')
        else:
            mld = mldclim['mld'][idoy:fdoy].mean('dayofyear')

        mld.to_netcdf(climmnfile)
    

def interpolate_oceanflux_data(idate, fdate, OUT_DIR):

    '''
    '''

    climmnfile = f"{OUT_DIR}ocflux.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc"
    if not os.path.exists(climmnfile):

        occlim = open_dataset(f'{CLIM_DIR}ocflux.daily.clim.1dg.1982-2010.nc')
        idoy = idate.timetuple().tm_yday
        fdoy = fdate.timetuple().tm_yday

        if idoy > fdoy:
            ocflux = concat((occlim['flux'][idoy:], occlim['flux'][:fdoy]), dim='dayofyear').mean('dayofyear')
        else:
            ocflux = occlim['flux'][idoy:fdoy].mean('dayofyear')

        ocflux.to_netcdf(climmnfile)


if __name__ == '__main__':

    args = arguments()
    init_date = args.date
    ndays = args.daysback
    date = datetime.strptime(init_date, '%Y%m%d')
    dback = date - relativedelta(days=ndays)
    
    foredir = f"{FORE_DIR}{date.strftime('%Y%m%d')}_{dback.strftime('%Y%m%d')}/"
    if not os.path.exists(foredir):
        os.makedirs(foredir)
    ## Download flux data and compute net surface flux
    create_sfcflux_data(dback, date, ndays, foredir)
    ## Download sst data
    create_mnsst_data(dback, date, ndays, foredir)
    ## time interpolation of mld data
    create_mld_data(dback, date, foredir)
    ## time interpolation of ocean flux data
    interpolate_oceanflux_data(dback, date, foredir)
