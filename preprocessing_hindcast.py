"""
Pre processing of hindcast data
"""
import os
import argparse
import numpy as np
from netCDF4 import Dataset
from datetime import datetime, timedelta
from xarray import open_mfdataset, DataArray, open_dataset, concat
from slab_ocean_model import som
from utils import OBS_DIR, SSTCLIM_DIR, FLXCLIM_DIR, CLIM_DIR, DIR_8210


def arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("--daysback", type=str, help="days back from init date")
    parser.add_argument("--lead", type=str, help="lead time in days")

    return parser.parse_args()


def compute_ndays_mean_obs(ndays):

    # SST
    filename_clim = f'{DIR_8210}sst.clim.{ndays}dmn.1dg.1982-2010.nc'
    filename_anom = f'{DIR_8210}sst.anom.{ndays}dmn.1dg.1982-2010.nc'
    if not os.path.exists(filename_anom):
        print ('reading sst climatology')
        dsclim = open_dataset(f'{CLIM_DIR}sst.daily.clim.1dg.1982-2010.nc')
        sstc = concat((dsclim['sst'][-(ndays//2):], dsclim['sst'], dsclim['sst'][:ndays//2]), dim='dayofyear')
        sstc = sstc.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]

        print ('reading sst obs data')
        ds = open_dataset(f'{SSTCLIM_DIR}sst.daily.1dg.1982-2010.nc')
        nwin = ds['sst'].shape[0]//ndays
        sst = ds['sst'].resample(time=f'{ndays}D', skipna=True, loffset=timedelta((ndays//2))).mean('time')[:nwin]
        doy_centered = sst['time.dayofyear'].values
        sstc = sstc.loc[doy_centered]

        print ('computing sst anomaly')
        ssta = sst.values - sstc.values

        print ('saving sst netcdfs')
        DataArray(sstc, dims=['time', 'lat', 'lon'], coords={'time': sst['time'], 'lat': sst['lat'], 'lon': sst['lon']}, name='sst').to_netcdf(filename_clim)
        DataArray(ssta, dims=['time', 'lat', 'lon'], coords={'time': sst['time'], 'lat': sst['lat'], 'lon': sst['lon']}, name='sst').to_netcdf(filename_anom)
    else:
        print (f'file {filename_anom} already exists')

    # SURFACE FLUX
    filename_clim = f'{DIR_8210}sfcflux.clim.{ndays}dmn.1dg.1982-2010.nc'
    filename_anom = f'{DIR_8210}sfcflux.anom.{ndays}dmn.1dg.1982-2010.nc'
    if not os.path.exists(filename_anom):
        print ('reading sfc flux climatology')
        dsclim = open_dataset(f'{CLIM_DIR}sfcflux.daily.clim.1dg.1982-2010.nc')
        fluxc = concat((dsclim['flux'][-(ndays//2):], dsclim['flux'], dsclim['flux'][:ndays//2]), dim='dayofyear')
        fluxc = fluxc.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]
        
        print ('reading sfc flux obs data')
        ds = open_dataset(f'{FLXCLIM_DIR}sfcflux.daily.1dg.1982-2010.nc')
        nwin = ds['flux'].shape[0]//ndays
        flux = ds['flux'].resample(time=f'{ndays}D', skipna=True, loffset=timedelta((ndays//2))).mean('time')[:nwin]
        doy_centered = flux['time.dayofyear'].values
        fluxc = fluxc.loc[doy_centered]

        print ('computing sfc flux anomaly')
        fluxa = flux.values - fluxc.values

        print ('saving sfc flux netcdf')
        DataArray(fluxc, dims=['time', 'lat', 'lon'], coords={'time': flux['time'], 'lat': flux['lat'], 'lon': flux['lon']}, name='flux').to_netcdf(filename_clim)
        DataArray(fluxa, dims=['time', 'lat', 'lon'], coords={'time': flux['time'], 'lat': flux['lat'], 'lon': flux['lon']}, name='flux').to_netcdf(filename_anom)
    else:
        print (f'file {filename_anom} already exists')

    # MLD
    filename_clim = f'{DIR_8210}mld.clim.{ndays}dmn.1dg.1982-2010.nc'
    if not os.path.exists(filename_clim):
        print ('reading mld climatology')
        dsclim = open_dataset(f'{CLIM_DIR}mld.daily.clim.1dg.1990-1992.nc')
        mldc = concat((dsclim['mld'][-(ndays//2):], dsclim['mld'], dsclim['mld'][:ndays//2]), dim='dayofyear')
        mldc = mldc.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]
        mldc = mldc.loc[doy_centered]

        print ('saving mld netcdf')
        DataArray(mldc.values, dims=['time', 'lat', 'lon'], coords={'time': flux['time'], 'lat': flux['lat'], 'lon': flux['lon']}, name='mld').to_netcdf(filename_clim)
    else:
        print (f'file {filename_clim} already exists')

    # OCEAN FLUX
    filename_clim = f'{DIR_8210}ocflux.clim.{ndays}dmn.1dg.1982-2010.nc'
    if not os.path.exists(filename_clim):
        print ('reading ocean flux climatology')
        dsclim = open_dataset(f'{CLIM_DIR}ocflux.daily.clim.1dg.1982-2010.nc')
        oflux = concat((dsclim['flux'][-(ndays//2):], dsclim['flux'], dsclim['flux'][:ndays//2]), dim='dayofyear')
        oflux = oflux.rolling(dayofyear=ndays, center=True).mean()[(ndays//2):-(ndays//2)]
        oflux = oflux.loc[doy_centered]
        
        print ('saving ocean flux netcdf')
        DataArray(oflux.values, dims=['time', 'lat', 'lon'], coords={'time': flux['time'], 'lat': flux['lat'], 'lon': flux['lon']}, name='flux').to_netcdf(filename_clim)
    else:
        print (f'file {filename_clim} already exists')


def compute_ndays_mean_model(ndays, lead):

    filename_model_clim = f'{DIR_8210}sst.model.clim.{ndays}dmn.{lead}dleadmn.1dg.1982-2010.nc'
    if not os.path.exists(filename_model_clim):
        sstc = open_dataset(f'{DIR_8210}sst.clim.{ndays}dmn.1dg.1982-2010.nc')
        csst = sstc['sst']
        sfc = open_dataset(f'{DIR_8210}sfcflux.clim.{ndays}dmn.1dg.1982-2010.nc')
        csflux = sfc['flux']
        mldc = open_dataset(f'{DIR_8210}mld.clim.{ndays}dmn.1dg.1982-2010.nc')
        mld = mldc['mld']
        occ = open_dataset(f'{DIR_8210}ocflux.clim.{ndays}dmn.1dg.1982-2010.nc')
        ocflux = occ['flux']

        model_clim = np.full((csst.shape[0], csst.shape[1], csst.shape[2]), np.nan)
        
        for n, i in enumerate(range(1, csst.shape[0])):

            sst_in = 0
            sflux_in = 0
            mld_in = mld[n]
            csflux_in = csflux[n]
            csst_in = csst[n]
            ocflux_in = ocflux[n]
        
            T__ = np.zeros((lead, csst.shape[1], csst.shape[2]))
        
            for m, j in enumerate(range(1, lead+1)):
            
                Tfore, delta = som(sflux_in, sst_in, mld_in, csflux_in, csst_in, ocflux_in, j)

                T__[m] = Tfore

            model_clim[i] = np.nanmean(T__, 0)

        DataArray(model_clim, dims=['time', 'lat', 'lon'], coords={'time': csst['time'], 'lat': csst['lat'], 'lon': csst['lon']}, name='sst').to_netcdf(filename_model_clim)


if __name__ == '__main__':

    args = arguments()
    ndays = int(args.daysback)
    lead = int(args.lead)
    compute_ndays_mean_obs(ndays)
    compute_ndays_mean_model(ndays, lead)

