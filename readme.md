Version 1:
========

Overview:
========

This is a simple linear slab ocean model that uses the past days average of SST and surface flux daily anomalies to estimate the average SST anomalies for a given lead time (in days).
The project is structured in hindcast and forecast sections:

**a) Hindcast:** pre-process the observational data, runs the hindcast and creates the verification images. To run the bash script:
>>>

$ `./compute_hindcast.sh` daysback lead
>>>

This section has the following scripts:

**filename**                      |**parameters**  |**description**
----------------------------------|----------------|----------------------------------------------------------------------------------
preprocessing_hindcast.py         |daysback, lead|Reads the observational 1982-2010 data, calculates the daily anomalies and then computes the time average of the anomalies. It also selects and takes the average of the same time slice for the climatological SST, MLD, Surface Flux and Ocean Flux. All the output netCDFs are 3D (time, lat, lon). 
ssta_hindcast.py                  |daysback, lead|Reads the pre-processed 1982-2010 data averaged for the past days (daysback) window and performs multiple lead days integrations. The predicted daily SST anomlay for a complete lead days integration is then avereged. 
hindcast_verification.py          |daysback, lead|Reads observed and predicted 1982-2010 data averaged for the past and lead days windows, respectively, and computes the correlation, RMSE, predicted/observed standard deviation ratio, hit rate (for each category), frequency of hits (for each catergory) and F1 score (overall). Figures are saved in results/hindcasts directory.

Here are some hindcast verifications for the 7, 15 and 31 lead days predictions (past and lead time windows are the same in this case):

![IMG](results/hindcasts/hind_7daysmn_7daysfore_verification.png)
![IMG](results/hindcasts/hind_15daysmn_15daysfore_verification.png)
![IMG](results/hindcasts/hind_31daysmn_31daysfore_verification.png)

**b) Forecast:** pre-process the observational data, runs the forecast and creates the forecast images. To run the bash script:
>>>

$ `./compute_forecast.sh` issue_date daysback lead
>>>

This section has the following scripts:

**filename**    |**parameters**            |**description**
----------------|--------------------------|----------------------------------------------------------------------------------
preprocessing.py|issue_date, daysback      | Downloads the previous past days (daysback) data of SST and surface flux, counting from the issue date (issue_date), calculates the daily anomalies and then computes the time average of the anomalies. It also selects and takes the average of the same time slice for the climatological SST, MLD, Surface Flux and Ocean Flux. All the output netCDFs are 2D (lat, lon).
ssta_forecast.py|issue_date, daysback, lead| Reads the pre-processed observational data and runs the model for the given lead time (lead). The predicted daily SST anomlay is averaged for the whole forecast period. Figures are saved in results/forecasts directory.

Here are some forecast examples:

![IMG](results/forecasts/ssta_20210227_7daysmn_7daylead.png)
![IMG](results/forecasts/ssta_20210306_7daysmn_7daylead.png)

**c) Other scripts:**

**filename**              |**parameters**|**description**
--------------------------|--------------|----------------------------------------------------------------------------------
compute_climatologies.py  |              |Creates the daily climatologies for SST, MLD, Sfc. flux and Oc. flux. It is also responsible for the downloads of the 1982-2010 SST and surface flux data and the creation of a single daily file for each of these variables.
utils.py                  |              |Defines and creates directories. It also defines the functions for interpolation, verification and plotting.
slab_ocean_model.py       |              |The model itself


Requirements:
========

- numpy==1.19.4

- netCDF4==1.5.5

- basemap==1.2.1

- xarray==0.16.2

- matplotlib==3.3.3

- wget==3.2

- request==2.22.0

Version 2 (TODO):
========

Couple the slab ocean model to a simple atmospheric model (shallow water model?) to also be able to prognostic the surface flux.
