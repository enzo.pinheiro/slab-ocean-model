
def som(f, sst, H, F=0, SST=0, Q=0, ndays=1):

    """
    f (2d array): surface heat flux anomaly
    sst (2d array): sst anomaly
    H (2d array): mixed layer depth ltm
    F (2d array): surface heat flux ltm
    SST (2d array): sst ltm
    Q (2d array): estimated internal ocean heat ltm
    ndays: time step in days
    """

    # Water Density
    rho = 1026
    # Heat capacity
    Co = 3930
    delta_SST = (((F+f)+Q)/(rho*Co*H)) * (ndays*86400)
    SSTpred = (SST+sst) + delta_SST

    return SSTpred, delta_SST
