import argparse
import numpy as np
from netCDF4 import Dataset
from datetime import datetime, timedelta
from mpl_toolkits.basemap import shiftgrid, interp, Basemap
from matplotlib import pyplot as plt
from slab_ocean_model import som
from utils import plot_field, OBS_DIR


def arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("--date", type=str, help="date to initialize the model in the format YYYYMMDD")
    parser.add_argument("--daysback", type=str, help="days back from init date to compute the average")
    parser.add_argument("--lead", type=str, help="number of days ahead to be estimated")

    return parser.parse_args()


def read_data(idate, fdate, foredir):

    nc1 = Dataset(foredir+f"sfcflmn.anom.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    sflux = nc1.variables['flux'][:]
    lat = nc1.variables['lat'][:]
    lon = nc1.variables['lon'][:]

    nc2 = Dataset(foredir+f"mldmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    mld = nc2.variables['mld'][:]

    nc3 = Dataset(foredir+f"sstmn.anom.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    sst = nc3.variables['sst'][:]

    nc4 = Dataset(foredir+f"sfcflmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    csflux = nc4.variables['flux'][:]

    nc5 = Dataset(foredir+f"sstmn.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    csst = nc5.variables['sst'][:]

    nc6 = Dataset(foredir+f"ocflux.clim.{idate.strftime('%Y%m%d')}-{fdate.strftime('%Y%m%d')}.nc")
    ocflux = nc6.variables['flux'][:]

    return sflux, mld, sst, csflux, csst, ocflux, lat, lon


if __name__ == '__main__':

    args = arguments()
    date = datetime.strptime(args.date, '%Y%m%d')
    ndaysback = int(args.daysback)
    lead = int(args.lead)
    dback = date - timedelta(days=ndaysback)
    ifore_date = (date + timedelta(days=1))
    ffore_date = (date + timedelta(days=lead))

    levels = [-3., -2.5, -2., -1.5, -1., -0.4, 0., 0.4, 1., 1.5, 2., 2.5, 3.]
    foredir = f"{OBS_DIR}forecasts/{date.strftime('%Y%m%d')}_{dback.strftime('%Y%m%d')}/"
    sflux, mld, sst, csflux, csst, ocflux, lat, lon = read_data(dback, date, foredir)
    Tfore, delta_T = som(sflux, sst, mld, csflux, csst, ocflux)

    Tfore = np.zeros((lead, lat.shape[0], lon.shape[0]))  
    Tforec = np.zeros((lead, lat.shape[0], lon.shape[0]))  
    for m, j in enumerate(range(1, lead+1)):
        
        Tfore[m, :], delta_T = som(sflux, sst, mld, csflux, csst, ocflux, j)
        Tforec[m, :], delta_Tc = som(0, 0, mld, csflux, csst, ocflux, j)
    
    Tfore = np.nanmean(Tfore, 0) - np.nanmean(Tforec, 0)

    fig = plt.figure(figsize=(16, 8))
    gs = fig.add_gridspec(2, 2)
    ax1 = fig.add_subplot(gs[0, 0])
    plot_field(sflux, lat, lon, np.arange(-100., 110., 10.), 'both', f"Surface heat flux anomaly for the period {dback.strftime('%d/%m/%Y')} - {date.strftime('%d/%m/%Y')}", 'RdBu_r', ax1)
    ax2 = fig.add_subplot(gs[0, 1])
    plot_field(sst, lat, lon, levels, 'both', f"Average of SST anomaly for the period {dback.strftime('%d/%m/%Y')} - {date.strftime('%d/%m/%Y')}", 'atsm1', ax2)
    ax3 = fig.add_subplot(gs[1, 0])
    plot_field(Tfore, lat, lon, levels, 'both', f"Average of SST anomaly predicted for the period {ifore_date.strftime('%d/%m/%Y')}-{ffore_date.strftime('%d/%m/%Y')}", 'atsm1', ax3)
    ax4 = fig.add_subplot(gs[1, 1])
    plot_field(Tfore-sst, lat, lon, np.arange(-1.5, 1.75, 0.25), 'both', 'Delta SSTa', 'RdBu_r', ax4)
    plt.savefig(f"results/forecasts/ssta_{date.strftime('%Y%m%d')}_{ndaysback}daysmn_{lead}daylead.png")
