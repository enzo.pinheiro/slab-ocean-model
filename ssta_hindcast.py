import argparse
import numpy as np
from xarray import DataArray
from netCDF4 import Dataset, num2date
from datetime import datetime, timedelta
from utils import DIR_8210, CLIM_DIR
from slab_ocean_model import som


def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--daysback", type=str, help="days back from init date")
    parser.add_argument("--lead", type=str, help="lead time in days")

    return parser.parse_args()


def read_data(ndays, lead):

    nc3 = Dataset(f"{DIR_8210}sst.anom.{ndays}dmn.1dg.1982-2010.nc")
    sst = nc3.variables['sst'][:]
    lat = nc3.variables['lat'][:]
    lon = nc3.variables['lon'][:]
    time = nc3.variables['time']
    dates = num2date(time[:], time.units, only_use_cftime_datetimes=False, only_use_python_datetimes=True)

    nc2 = Dataset(f'{DIR_8210}sst.clim.{ndays}dmn.1dg.1982-2010.nc')
    csst = nc2.variables['sst'][:]
    
    nc1 = Dataset(f"{DIR_8210}sfcflux.anom.{ndays}dmn.1dg.1982-2010.nc")
    sflux = nc1.variables['flux'][:]

    nc4 = Dataset(f'{DIR_8210}sfcflux.clim.{ndays}dmn.1dg.1982-2010.nc')
    csflux = nc4.variables['flux'][:]

    nc5 = Dataset(f'{DIR_8210}mld.clim.{ndays}dmn.1dg.1982-2010.nc')
    mld = nc5.variables['mld'][:]

    nc6 = Dataset(f'{DIR_8210}ocflux.clim.{ndays}dmn.1dg.1982-2010.nc')
    ocflux = nc6.variables['flux'][:]

    nc7 = Dataset(f'{DIR_8210}sst.model.clim.{ndays}dmn.{lead}dleadmn.1dg.1982-2010.nc')
    csstm = nc7.variables['sst'][:]

    return sflux, sst, csst, csflux, mld, ocflux, csstm, time, dates, lat, lon


if __name__ == '__main__':

    args = arguments()
    ndays = int(args.daysback)
    lead = int(args.lead)

    hind_fname = f'{DIR_8210}sst.anom.hindcast.{ndays}dmn.{lead}dleadmn.1dg.1982-2010.nc'

    sflux, sst, csst, csflux, mld, ocflux, csstm, time, dates, lat, lon = read_data(ndays, lead)

    T_ = np.full((sst.shape[0], sst.shape[1], sst.shape[2]), np.nan)
    
    for n, i in enumerate(dates[0:-1]):

        tmday = i.timetuple().tm_yday -1

        sst_in = sst[n]
        sflux_in = sflux[n]
        mld_in = mld[n]
        csflux_in = csflux[n]
        csst_in = csst[n]
        ocflux_in = ocflux[n]
    
        T__ = np.zeros((lead, sst.shape[1], sst.shape[2]))
    
        for m, j in enumerate(range(1, lead+1)):
           
            Tfore, delta = som(sflux_in, sst_in, mld_in, csflux_in, csst_in, ocflux_in, j)
            
            T__[m] = Tfore

        T_[n+1] = np.nanmean(T__, 0)

    T_ = T_ - csstm

    DataArray(T_, dims=['time', 'lat', 'lon'], coords={'time': dates, 'lat': lat, 'lon': lon}, name='sst').to_netcdf(hind_fname)
