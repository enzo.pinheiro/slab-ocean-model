import os
import numpy as np
import matplotlib.colors as clr
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap, interp


OBS_DIR = 'data/'

SST_DIR = f'{OBS_DIR}oisst/'
if not os.path.exists(SST_DIR):
        os.makedirs(SST_DIR)
FLX_DIR = f'{OBS_DIR}ncep1/'
if not os.path.exists(FLX_DIR):
        os.makedirs(FLX_DIR)
SSTCLIM_DIR = f'{OBS_DIR}oisst/8210/'
if not os.path.exists(SSTCLIM_DIR):
        os.makedirs(SSTCLIM_DIR)
FLXCLIM_DIR = f'{OBS_DIR}ncep1/8210/'
if not os.path.exists(FLXCLIM_DIR):
        os.makedirs(FLXCLIM_DIR)
CLIM_DIR = f'{OBS_DIR}clims/'
if not os.path.exists(CLIM_DIR):
        os.makedirs(CLIM_DIR)
DIR_8210 = f'{OBS_DIR}8210/'
if not os.path.exists(DIR_8210):
        os.makedirs(DIR_8210)
FORE_DIR = f'{OBS_DIR}forecasts/'
if not os.path.exists(FORE_DIR):
        os.makedirs(FORE_DIR)
FIGS_HIND_DIR = f'results/hindcasts/'
if not os.path.exists(FIGS_HIND_DIR):
        os.makedirs(FIGS_HIND_DIR)
FIGS_FORE_DIR = f'results/forecasts/'
if not os.path.exists(FIGS_FORE_DIR):
        os.makedirs(FIGS_FORE_DIR)


def rmse(predictions, targets, axis=-1):
    #  Got from https://stackoverflow.com/questions/17197492/is-there-a-library-function-for-root-mean-square-error-rmse-in-python

    differences = predictions - targets                            #the DIFFERENCEs.

    differences_squared = differences ** 2                         #the SQUAREs of ^
    
    if axis == -1:                                                   #the MEAN of ^
        mean_of_differences_squared = np.nanmean(differences_squared)  
    else:
        mean_of_differences_squared = np.nanmean(differences_squared, axis=axis)

    rmse_val = np.sqrt(mean_of_differences_squared)                #ROOT of ^

    return rmse_val                                                #get the ^


def interpolate2grid(variable, lat_values, lon_values, new_lat, new_lon):
    # Must be 3d array (time, lat, lon)

    x, y = np.meshgrid(new_lon, new_lat)

    ntime = variable.shape[0]

    new_data = np.zeros((ntime, new_lat.shape[0], new_lon.shape[0]))

    for i in range(0, ntime):
        new_data[i, :, :] = interp(variable[i, :, :], lon_values, lat_values, x, y, order=1)

    return new_data


def plot_field(field, lat, lon, levels, extend, title, cmap, ax=None):

    if cmap == 'atsm1':
        atsm1 = ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82')
        my_cmap = clr.ListedColormap(atsm1)
        if extend == 'both':
            cmapover = atsm1[-1]
            cmapunder = atsm1[0]
            bar = atsm1[1:-1]
            my_cmap = clr.ListedColormap(bar)
            my_cmap.set_over(cmapover)
            my_cmap.set_under(cmapunder)
        elif extend == 'max':
            cmapover = atsm1[-1]
            bar = atsm1[0:-1]
            my_cmap = clr.ListedColormap(bar)
            my_cmap.set_over(cmapover)
        elif extend == 'min':
            cmapunder = atsm1[0]
            bar = atsm1[1:]
            my_cmap = clr.ListedColormap(bar)
            my_cmap.set_under(cmapunder)
    else:
        my_cmap = plt.set_cmap(cmap)

    # plt.figure()
    plt.title(title)
    x, y = np.meshgrid(lon, lat)
    m = Basemap(-90, -25, 30, 25, ax=ax)
    m.fillcontinents()
    cs = m.contourf(x, y, field, cmap=my_cmap, levels=levels, extend=extend)
    plt.colorbar(cs, orientation='horizontal', ticks=levels)
